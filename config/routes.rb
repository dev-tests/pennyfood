Rails.application.routes.draw do
  resources :recipes, only: :show

  root to: 'recipes#index'
end
