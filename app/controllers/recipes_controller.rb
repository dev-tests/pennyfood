class RecipesController < ApplicationController
  before_action :set_recipe, only: :show

  def index
    results = search(Recipe.by_best_ratings)
    @recipes = paginate(results)
  end

  def show
  end

  private

  def set_recipe
    @recipe = Recipe.find(params[:id])
  end

  def paginate(relation)
    @pagy, records = pagy(relation, items: 24)
    records
  end

  def search(relation)
    return relation unless search_params

    relation.search_all_matching_ingredients(search_params)
  end

  def search_params
    params[:query]
  end
end
