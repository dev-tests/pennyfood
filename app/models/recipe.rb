class Recipe < ApplicationRecord
  scope :by_best_ratings, -> { order('average_rating DESC NULLS LAST') }

  def self.import(io)
    now = Time.current

    recipes_attributes = io.readlines.map do |line|
      properties = JSON.parse(line, symbolize_names: true)

      {
        name: properties[:name],
        author: properties[:author],
        image_url: properties[:image],
        difficulty: properties[:difficulty],
        budget: properties[:budget],
        servings: properties[:people_quantity],
        preparation_time: properties[:prep_time],
        cooking_time: properties[:cook_time],
        total_time: properties[:total_time],
        average_rating: properties[:rate],
        ingredients: properties[:ingredients],
        created_at: now,
        updated_at: now
      }
    end

    Recipe.insert_all recipes_attributes
  end

  # Search recipe that are possible to make with the provided ingredients
  # if a recipe ingredient is not in the list the recipe will not be returned
  #
  # @param ingredients [String] a comma separated list of ingredients
  def self.search_all_matching_ingredients(ingredients)
    ingredients = ingredients.split(',').map do |ingredient|
      "%#{ingredient.strip}%"
    end

    str = <<~SQL
      ingredients = array(
        select ingredient from unnest(ingredients) as ingredient
        where ingredient ILIKE ANY(array[:ingredients])
      )
    SQL

    self.where(str, ingredients: ingredients)
  end

  def image_url
    super.presence || 'default-unsplash.jpg'
  end
end
