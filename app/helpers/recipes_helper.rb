module RecipesHelper
  def rating_stars(recipe)
    rating = recipe.average_rating.to_i.floor
    rating_label = "Rating: #{rating} out of 5 stars"

    tag.div(class:'leading-none text-green-300 flex items-center', role: 'img', aria: { label: rating_label }) do
      capture do
        5.times do |n|
          icon = if n < rating
            'icons/star-full.svg'
          else
            'icons/star-empty.svg'
          end

          concat inline_svg_tag(icon, class: 'w-3.5 h-3.5')
        end
      end
    end
  end
end
