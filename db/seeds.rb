# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

datasource = File.expand_path('recipes.json', __dir__)

if File.exist? datasource
  Recipe.delete_all

  Recipe.import File.new(datasource)
end
