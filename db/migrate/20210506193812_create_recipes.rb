class CreateRecipes < ActiveRecord::Migration[6.1]
  def change
    create_table :recipes do |t|
      t.string :name
      t.string :author
      t.string :image_url
      t.string :difficulty
      t.string :budget
      t.string :servings
      t.string :preparation_time
      t.string :cooking_time
      t.string :total_time
      t.decimal :average_rating

      t.timestamps
    end
  end
end
