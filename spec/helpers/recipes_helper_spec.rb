require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the RecipesHelper. For example:
#
# describe RecipesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe RecipesHelper, type: :helper do
  describe '#rating_stars' do
    let(:recipe) { build :recipe, average_rating: 4.2 }

    it 'renders the average rating with stars svg' do
      actual = helper.rating_stars(recipe)

      expect(actual).to have_selector('svg.icon-star-full', count: 4)
      expect(actual).to have_selector('svg.icon-star-empty', count: 1)
    end

    it 'sets a label for screen readers' do
      actual = helper.rating_stars(recipe)

      expect(actual).to have_selector('div[role="img"][aria-label="Rating: 4 out of 5 stars"]')
    end
  end
end
