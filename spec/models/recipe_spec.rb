require 'rails_helper'

RSpec.describe Recipe,
type: :model do
  describe '.import' do
    subject(:import) { described_class.import file_fixture('sample.json') }

    it 'creates 3 recipes' do
      expect { import }.to change { Recipe.count }.by(3)
    end

    it 'correcty defines recipes attributes' do
      import

      recipe = Recipe.first

      expect(recipe).to have_attributes(
        name: '6 ingrédients que l’on peut ajouter sur une crêpe au Nutella®',
        author: 'Nutella',
        image_url: 'https://assets.afcdn.com/recipe/20171006/72810_w420h344c1cx2544cy1696cxt0cyt0cxb5088cyb3392.jpg',
        difficulty: 'très facile',
        budget: 'bon marché',
        servings: '6',
        preparation_time: '15 min',
        cooking_time: '10 min',
        total_time: '25 min',
        average_rating: 5,
        ingredients: [
          "600g de pâte à crêpe",
          "1/2 orange",
          "1/2 banane",
          "1/2 poire pochée",
          "1poignée de framboises",
          "75g de Nutella®",
          "1poignée de noisettes torréfiées",
          "1/2poignée d'amandes concassées",
          "1cuillère à café d'orange confites en dés",
          "2cuillères à café de noix de coco rapée",
          "1/2poignée de pistache concassées",
          "2cuillères à soupe d'amandes effilées"
        ]
      )
    end

    it 'parses each line' do
      import

      expect(Recipe.pluck(:name)).to contain_exactly(
        "6 ingrédients que l’on peut ajouter sur une crêpe au Nutella®",
        "Agneau à l'abricot (recette turque)",
        "Agneau à l'oriental"
      )
    end
  end

  describe '.search_all_matching_ingredients' do
    subject { described_class.search_all_matching_ingredients(query) }

    let!(:recipe_1) do
      create :recipe, ingredients: [
        'Spaghetti',
        'Boeuf haché',
        'Bolognaise',
        'Emmental'
      ]
    end

    let!(:recipe_2) do
      create :recipe, ingredients: [
        'Tagliatelles',
        'Lardons',
        'Crème',
        'Emmental'
      ]
    end

    before do
      create :recipe, ingredients: [
        'Macaroni',
        'Cheddar'
      ]
    end

    context 'when query contains one ingredient' do
      let(:query) { 'emmental' }

      it { is_expected.to be_empty }
    end

    context 'when query contains all ingredient of a recipe' do
      let(:query) { 'tagliatelles, lardons, crème, emmental' }

      it { is_expected.to contain_exactly(recipe_2) }
    end

    context 'when query contains ingredients for multiple recipes' do
      let(:query) { (recipe_1.ingredients + recipe_2.ingredients).join(',') }

      it { is_expected.to match_array([recipe_1, recipe_2]) }
    end
  end

  describe '#image_url' do
    subject { model.image_url }

    context 'when recipe has an image url' do
      let(:model) { build :recipe, image_url: 'https://via.placeholder.com/150' }

      it { is_expected.to eq 'https://via.placeholder.com/150' }
    end

    context 'when recipe does not have an image url' do
      let(:model) { build :recipe, image_url: nil }

      it { is_expected.to eq 'default-unsplash.jpg' }
    end
  end
end
