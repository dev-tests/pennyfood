require 'rails_helper'

RSpec.describe "ListingRecipes", type: :system do
  let!(:recipe_1) { create :recipe, name: 'the only one', image_url: 'https://via.placeholder.com/150', average_rating: 4.5 }
  let!(:recipe_2) { create :recipe, image_url: nil, ingredients: ['Des smarties'] }

  before do
    driven_by(:rack_test)
  end

  it 'displays a list of recipes' do
    visit root_path

    expect(page).to have_selector('article', count: 2)

    within("#recipe_#{recipe_1.id}") do
      expect(page).to have_selector("img[src*='https://via.placeholder.com/150']")
      expect(page).to have_selector("h3", text: 'The only one')
      expect(page).to have_selector("div[role='img'][aria-label='Rating: 4 out of 5 stars']")
    end
  end

  it 'searches recipes with matching ingredients' do
    visit root_path

    within('form') do
      fill_in 'List of ingredients', with: 'Des smarties'
      click_button 'search_button'
    end

    expect(current_url).to eq root_url(query: 'Des smarties')

    expect(page).to have_selector("#recipe_#{recipe_2.id}")
    expect(page).to have_no_selector("#recipe_#{recipe_1.id}")
  end

  it 'paginates recipes' do
    create_list :recipe, 25

    visit root_path

    expect(page).to have_selector('article', count: 24)

    click_link '2'

    expect(current_url).to eq root_url(page: 2)
    expect(page).to have_selector('article', count: 3)
  end

  it 'displays a recipe details', :aggregate_failures do
    visit root_path

    expect(page).to have_selector('article', count: 2)

    within("#recipe_#{recipe_1.id}") do
      click_link('show_recipe')
    end

    expect(current_url).to eq recipe_url(recipe_1)

    within('#recipe_header') do
      expect(page).to have_selector("h2", text: 'The only one')
      expect(page).to have_selector("p", text: recipe_1.author)
      expect(page).to have_selector("div[role='img'][aria-label='Rating: 4 out of 5 stars']")
    end

    within('#recipe_details') do
      expect(page).to have_selector("dd", text: recipe_1.difficulty)
      expect(page).to have_selector("dd", text: recipe_1.budget)
      expect(page).to have_selector("dd", text: recipe_1.servings)
      expect(page).to have_selector("dd", text: recipe_1.preparation_time)
      expect(page).to have_selector("dd", text: recipe_1.cooking_time)
      expect(page).to have_selector("dd", text: recipe_1.total_time)
    end

    within('#recipe_ingredients') do
      recipe_1.ingredients.each do |ingredient|
        expect(page).to have_selector("li", text: ingredient)
      end
    end
  end
end
